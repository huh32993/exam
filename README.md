# 在线考试
[演示地址](http://47.92.221.134:8080/exam)

## 考试
![输入图片说明](https://images.gitee.com/uploads/images/2019/1217/223736_b4bc2ac9_393390.png "屏幕截图.png")
## 编辑试题
![输入图片说明](https://images.gitee.com/uploads/images/2019/0816/100128_6a0e1152_393390.png)
## 导入试题
![输入图片说明](https://images.gitee.com/uploads/images/2019/0816/095158_81637199_393390.png)

## 后台界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/0816/095447_6c4d357c_393390.png)

## 测试账号
系统管理员	sysadmin	111111

## 技术实现
bs架构，采用开源组件spring2.1.3、mysql5.5、jdk7、easyui1.4.5、bootstrap3.3.7

## qq群
811189776